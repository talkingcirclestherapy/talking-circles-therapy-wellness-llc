The goal of our independently licensed therapists is to meet you where you are while providing the guidance needed to discover your best self. All of our therapists are highly trained and possess years of experience. We believe in a holistic approach that addresses all of you; physically, mentally, emotionally and spiritually.

Address: 4004 Carlisle Blvd NE, Suite A-2, Albuquerque, NM 87107, USA

Phone: 505-261-9770

Website: http://www.talkingcirclestherapy.com
